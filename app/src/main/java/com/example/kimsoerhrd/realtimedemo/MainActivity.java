package com.example.kimsoerhrd.realtimedemo;

import android.app.Application;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;


import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;


public class MainActivity extends AppCompatActivity {

    EditText inputMessage;
    TextView user, mMessage;
    Socket mSocket;
    Message myMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        inputMessage = findViewById(R.id.etMessage);
        user = findViewById(R.id.tvUsername);
        mMessage = findViewById(R.id.tvMessage);
        MyApp myApp = (MyApp) getApplication();
        mSocket = myApp.getmSocket();

        mSocket.on(Socket.EVENT_CONNECT, onSocketConnected);
        mSocket.on(Socket.EVENT_DISCONNECT, onSocketDisconnected);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onSocketConnectedError);
        mSocket.on("message", onNewMessage);

        mSocket.connect();
    }

    public void onChatting(View view) {
        attemptSend();
    }
    private void attemptSend() {
        String message = inputMessage.getText().toString().trim();
        if (TextUtils.isEmpty(message)) {
            return;
        }



        inputMessage.setText("");

        addMessage("kimsoer", message);
        JSONObject data= new JSONObject();
        try{
            data.put("username", myMessage.getUsername());
            data.put("message", myMessage.getMessage());
        }catch (JSONException e){

        }

        mSocket.emit("message", data);
    }

    Emitter.Listener onSocketConnectedError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.e("Connection Error","Connection Error");
        }
    };
    Emitter.Listener onSocketDisconnected = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.e("Disconnected", "Disconnection");
        }
    };

    Emitter.Listener onSocketConnected = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.e("Connect","Connection Success");
        }
    };
    private Emitter.Listener onNewMessage = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {
            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username="";
                    String message="";

                    try {
                        username = data.getString("username");
                        message = data.getString("message");


                    } catch (JSONException e) {
                        Log.e("Error", e.getMessage());
                    }

                    addMessage(username, message);
                   // mSocket.emit("message", myMessage);
                }
            });
        }
    };

    private void addMessage(String username, String message) {
        user.setText(username);
        mMessage.setText(message);
         myMessage = new Message();
         myMessage.setUsername(username);
         myMessage.setMessage(message);


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSocket.disconnect();
        mSocket.off("message", onNewMessage);
    }
}
