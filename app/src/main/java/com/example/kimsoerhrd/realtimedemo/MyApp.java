package com.example.kimsoerhrd.realtimedemo;

import android.app.Application;

import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import java.net.URI;
import java.net.URISyntaxException;

public class MyApp extends Application {



    private Socket mSocket;
    {
        try {
            IO.Options options = new IO.Options();
            options.forceNew = true;
            options.reconnection = false;
            mSocket = IO.socket("http://192.168.178.16:3000/chat", options);


        } catch (URISyntaxException e) {}
    }

    public Socket getmSocket(){
        return mSocket;
    }
}
